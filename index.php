<?php
/**
* @author Mathis Faivre <mathis.faivre@protonmail.com>
*/
    error_reporting(E_ALL);
    ini_set('display_errors', 1);
    require_once "./controleur/routeur.php";

    $routeur = new Routeur();

    $routeur->routerRequete();
?>

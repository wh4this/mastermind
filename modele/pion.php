<?php

/**
* @author Mathis Faivre <mathis.faivre@protonmail.com>
*/
class Pion {
  private $couleur;
  private $couleursPossible;

  public function __construct() {
    $this->couleursPossible = array("rouge", "jaune", "vert", "bleu", "orange", "blanc", "violet", "fuchsia");
  }

  public function setCouleur($couleur) {
    if(in_array($couleur, $this->couleursPossible)) {
      $this->couleur = $couleur;
    } else {
      echo "La couleur n'existe pas.";
    }
  }

  public function getCouleur() {
    return $this->couleur;
  }
}

?>

<?php

/**
* @author Mathis Faivre <mathis.faivre@protonmail.com>
*/
class Partie {
  private $nbrCoups;
  private $partieGagnee;

  public function __construct() {
    $this->nbrCoups = 0;
    $this->partieGagnee = 0;
  }

  public function getPartieGagnee() {
    return $this->partieGagnee;
  }

  public function getNbrCoups() {
    return $this->nbrCoups;
  }

  public function incrementeCoup() {
    $this->nbrCoups = $this->nbrCoups + 1;
  }

  public function setPartieGagnee($valeur) {
    if($valeur == 0 || $valeur == 1) {
      $this->partieGagnee = $valeur;
    } else {
      echo "la valeur est incorrect";
    }
  }

}

?>
